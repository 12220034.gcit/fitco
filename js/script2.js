

const menu = document.querySelector('.menu');
const container = document.querySelector('.container');
const slides = document.querySelectorAll('.slide');

let currentSlide = 0;

menu.addEventListener('click', () => {
  container.classList.toggle('open');
});

container.addEventListener('scroll', () => {
  const slideHeight = slides[currentSlide].offsetHeight;
  const scrollTop = container.scrollTop;
  currentSlide = Math.floor(scrollTop / slideHeight);
  setActiveSlide();
});

function setActiveSlide() {
  slides.forEach((slide, index) => {
    if (index === currentSlide) {
      slide.classList.add('active');
    } else {
      slide.classList.remove('active');
    }
  });
}
