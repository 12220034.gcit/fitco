
const container = document.querySelector('.container');
const sections = document.querySelectorAll('.pages');

let currentSlide = 0;

container.addEventListener('scroll', () => {
  const slideHeight = sections[currentSlide].offsetHeight;
  const scrollTop = container.scrollTop;
  currentSlide = Math.floor(scrollTop / slideHeight);
  setActiveSlide();
});

function setActiveSlide() {
  sections.forEach((pages, index) => {
    if (index === currentSlide) {
      pages.classList.add('active');
    } else {
      pages.classList.remove('active');
    }
  });
}
